import * as React from 'react';

const MessageArchiving = (props: React.SVGProps<SVGSVGElement>) => (
  <svg
    {...props}
    xmlns="http://www.w3.org/2000/svg"
    width="17"
    height="17"
    viewBox="0 0 17 17"
  >
    <g>
      <path
        fill="rgb(0,153,207)"
        fill-rule="evenodd"
        d="M9 11.27729636v-6.978H8v6.98l-2.857-3.075-.733.682 4.089 4.398 4.09-4.398-.733-.682-2.856 3.073z"
      />
      <path
        fill="rgb(0,153,207)"
        fill-rule="evenodd"
        d="M0 0h11.70898438C14.62597655 0 17 2.37402344 17 5.29101563V17H0V0zm1 16h15V5.29101562C16 2.9249878 14.07495117 1 11.70898437 1H1v15z"
      />
    </g>
  </svg>
);

export default MessageArchiving;

import * as React from 'react';

const Asset10 = (props: React.SVGProps<SVGSVGElement>) => (
  <svg
    {...props}
    xmlns="http://www.w3.org/2000/svg"
    width="15.828"
    height="16.541"
    viewBox="0 0 15.828 16.541"
  >
    <g>
      <rect
        width=".879"
        height="10.341"
        x="5.944"
        y="3.166"
        fill="rgb(65,64,66)"
        rx="0"
        ry="0"
      />
      <rect
        width=".879"
        height="10.341"
        x="2.875"
        y="3.166"
        fill="rgb(65,64,66)"
        rx="0"
        ry="0"
      />
      <rect
        width=".879"
        height="10.341"
        x="9.005"
        y="3.166"
        fill="rgb(65,64,66)"
        rx="0"
        ry="0"
      />
      <rect
        width=".879"
        height="10.341"
        x="12.073"
        y="3.166"
        fill="rgb(65,64,66)"
        rx="0"
        ry="0"
      />
      <path
        fill="rgb(65,64,66)"
        fill-rule="evenodd"
        d="M15.82825244 3.32393301h-.87934736V.87934736H.87934736V3.323933H0V0h15.82825244v3.32393301z"
      />
      <rect
        width="10.517"
        height=".879"
        x="2.656"
        y="15.661"
        fill="rgb(65,64,66)"
        rx="0"
        ry="0"
      />
    </g>
  </svg>
);

export default Asset10;

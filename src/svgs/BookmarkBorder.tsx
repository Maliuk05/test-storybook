import * as React from 'react';

const BookmarkBorder = (props: React.SVGProps<SVGSVGElement>) => (
  <svg
    {...props}
    xmlns="http://www.w3.org/2000/svg"
    width="14.72"
    height="15.026"
    viewBox="0 0 14.72 15.026"
    stroke="currentColor"
  >
    <path
      fill="#2D393D"
      fill-rule="evenodd"
      d="M0 0v15.02636027l7.36000013-6.79236012 7.36000014 6.79236012V.05704h-.92000002v12.86896023l-6.44000012-5.9450401-6.44000011 5.9450401V.92000002h9.20000016V0H0z"
    />
  </svg>
);

export default BookmarkBorder;

import * as React from 'react';

const Asset15 = (props: React.SVGProps<SVGSVGElement>) => (
  <svg
    {...props}
    xmlns="http://www.w3.org/2000/svg"
    width="16.708"
    height="18.722"
    viewBox="0 0 16.708 18.722"
  >
    <g>
      <path
        fill="rgb(65,64,66)"
        fill-rule="evenodd"
        d="M16.7075998 16.1135518H0V6.86867992h6.14468392v.92819998H.9282v7.38847191H15.7793998v-7.3884719h-5.33714994v-.9282h6.26534992v9.2448719z"
      />
      <rect
        width=".928"
        height="10.832"
        x="7.89"
        y=".659"
        fill="rgb(65,64,66)"
        rx="0"
        ry="0"
      />
      <path
        fill="rgb(65,64,66)"
        fill-rule="evenodd"
        d="M4.56674394 5.09581794l-.65902199-.64974L8.3537999 0l4.44607794 4.44607795-.65902199.64973999L8.3537999 1.30876198 4.56674394 5.09581794z"
      />
      <rect
        width="6.182"
        height=".928"
        x="5.263"
        y="17.794"
        fill="rgb(65,64,66)"
        rx="0"
        ry="0"
      />
    </g>
  </svg>
);

export default Asset15;

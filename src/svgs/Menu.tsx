import * as React from 'react';

const Menu = (props: React.SVGProps<SVGSVGElement>) => (
  <svg
    {...props}
    xmlns="http://www.w3.org/2000/svg"
    width="16"
    height="14.868"
    viewBox="0 0 16 14.868"
    stroke="currentColor"
  >
    <g>
      <path
        fill="#2D393D"
        fill-rule="evenodd"
        d="M5.5467 13.8681v1h10.453v-1H5.5467z"
      />
      <path
        fill="#2D393D"
        fill-rule="evenodd"
        d="M5.5467 6.9345v1h10.453v-1H5.5467z"
      />
      <path
        fill="#2D393D"
        fill-rule="evenodd"
        d="M5.5467 0v1h10.453V0H5.5467z"
      />
      <path fill="#2D393D" fill-rule="evenodd" d="M0 13.8681v1h2.132v-1H0z" />
      <path fill="#2D393D" fill-rule="evenodd" d="M0 6.9345v1h2.132v-1H0z" />
      <path fill="#2D393D" fill-rule="evenodd" d="M0 0v1h2.132V0H0z" />
    </g>
  </svg>
);

export default Menu;

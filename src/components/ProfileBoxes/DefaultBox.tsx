import React, { FC } from 'react';

//types
import { DefaultBoxProps } from '../../types/common';

//components
import { FileUploader } from '../FileUploader';

//elements
import { Input } from '../../elements/Input';
import { Button } from '../../elements/Button';

const HelpText: string =
  'Use commas to separate multiple roles. Your roles are shown next to your name on your chat messages.';

export const DefaultBox: FC<DefaultBoxProps> = ({
  handleSubmit,
  handleInputChange,
  fullName,
  nickName,
  email,
  role,
  url,
}) => {
  return (
    <>
      <div className="flex ml-6 py-4">
        <FileUploader />
        <div className="ml-4 mt-2">
          <span className="text-md text-gray-800">XMPP Address (JID):</span>
          <p className="text-sm text-gray-600 mt-2">
            5620@bloqzone.goodbytes.im
          </p>
        </div>
      </div>
      <form className="px-6" onSubmit={handleSubmit}>
        <Input
          type="text"
          label="Full Name:"
          value={fullName}
          inputTextWeight="font-hairline"
          // inputTextColor='text-md'
          labelFont="font-semibold"
          labelColor="text-gray-600"
          name="fullName"
          margin="mt-4"
          onChange={handleInputChange}
        />
        <Input
          type="text"
          label="NickName:"
          labelColor="text-gray-600"
          labelFont="font-semibold"
          value={nickName}
          margin="mt-4"
          name="nickName"
          onChange={handleInputChange}
        />
        <Input
          type="text"
          label="URL:"
          labelColor="text-gray-600"
          labelFont="font-semibold"
          value={url}
          name="url"
          margin="mt-4"
          onChange={handleInputChange}
        />
        <Input
          type="text"
          label="Email:"
          labelColor="text-gray-600"
          labelFont="font-semibold"
          value={email}
          margin="mt-4"
          name="email"
          onChange={handleInputChange}
        />
        <Input
          type="text"
          value={role}
          margin="mt-4 mb-4"
          label="Role:"
          labelColor="text-gray-600"
          labelFont="font-semibold"
          name="role"
          onChange={handleInputChange}
          helpText={HelpText}
          helpTextStyles="text-xs text-gray-500 font-mono mt-2 ml-2"
        />
        <hr />
        <Button
          type="submit"
          margin="mt-8 mb-8"
          title="Save and Close"
          labelColor="text-gray-600"
          backgroundColor="bg-blue-700"
          hover="bg-blue-800"
          rounded={5}
        />
      </form>
    </>
  );
};

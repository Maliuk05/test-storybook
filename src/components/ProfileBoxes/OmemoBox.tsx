import React, { FC } from 'react';

//types
import { OmemoBoxProps } from '../../types/common';

//elements
import { Button } from '../../elements/Button';

export const OmemoBox: FC<OmemoBoxProps> = ({
  deviceKey,
  handleGenerateKey,
}) => {
  return (
    <div className="px-8">
      <div className="flex flex-col border mt-8 rounded">
        <span className="bg-blue-600 p-2 text-white text-sm">
          This device's OMEMO fingerprint
        </span>
        <span className="px-2 py-2 text-gray-600 text-xs">{deviceKey}</span>
      </div>
      <Button
        title="Generate new keys and fingerprint"
        margin="mt-8 mb-24"
        backgroundColor="bg-red-600"
        textColor="text-white"
        hover="bg-red-800"
        rounded={4}
        width="w-5/6"
        textSize="text-lg"
        onClick={handleGenerateKey}
      />
    </div>
  );
};

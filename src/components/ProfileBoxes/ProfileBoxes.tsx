import React, { FC, useState, useEffect, FormEvent, ChangeEvent } from 'react';

// types
import { ProfileData } from '../../types/common';

//components
import { DefaultBox } from './DefaultBox';
import { OmemoBox } from './OmemoBox';

//elements
import { Button } from '../../elements/Button';

//svg
import Close from '../../svgs/Close';

type ProfileBoxesProps = {
  data: ProfileData;
};

export const ProfileBoxes: FC<ProfileBoxesProps> = ({ data }) => {
  const [formData, setFormData] = useState<ProfileData>({
    fullName: '',
    nickName: '',
    url: '',
    email: '',
    role: '',
  });

  const [toggleMenu, setToggleMenu] = useState<boolean>(false);

  useEffect(() => {
    setFormData({
      fullName: data.fullName,
      nickName: data.nickName,
      url: data.url,
      email: data.email,
      role: data.role,
    });
  }, [data.fullName, data.nickName, data.role, data.email, data.url]);

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    alert(JSON.stringify(formData));
  };

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>): void => {
    const {
      target: { value: inputText, name: inputName },
    } = e;

    setFormData({ ...formData, [inputName]: inputText });
  };

  const handleGenerateKey = (): void => {
    alert('generate handler!');
  };

  const { fullName, nickName, email, role, url } = formData;
  // style={{ height: 700, overflow: 'auto' }}
  return (
    <div className="border pt-4">
      <div className="flex justify-between px-4 mb-4">
        <span className="text-xl text-gray-600">Your Profile</span>
        <button className="focus:outline-none" onClick={() => alert('closed')}>
          <Close />
        </button>
      </div>
      <hr />
      <div className="flex justify-center mt-4">
        <Button
          title="Profile"
          textColor={!toggleMenu ? 'text-white' : 'text-blue-500'}
          backgroundColor={!toggleMenu ? 'bg-blue-500' : 'bg-white'}
          onClick={() => setToggleMenu(false)}
          hover={!toggleMenu ? 'bg-blue-600' : ''}
          rounded={4}
        />
        <Button
          backgroundColor={toggleMenu ? 'bg-blue-500' : 'bg-white'}
          textColor={toggleMenu ? 'text-white' : 'text-blue-500'}
          rounded={4}
          title="OMEMO"
          hover={toggleMenu ? 'bg-blue-600' : ''}
          onClick={() => setToggleMenu(true)}
        />
      </div>
      {!toggleMenu ? (
        <DefaultBox
          email={email}
          fullName={fullName}
          nickName={nickName}
          role={role}
          url={url}
          handleInputChange={handleInputChange}
          handleSubmit={handleSubmit}
        />
      ) : (
        <OmemoBox
          deviceKey="87a33bd0 4115a8e3 2618cb1f e17f7234 c4f8b433 28f02b09 51ed32ff 22639422"
          handleGenerateKey={handleGenerateKey}
        />
      )}
    </div>
  );
};

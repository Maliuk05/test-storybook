import React, { FC } from 'react';

export const FooterSidebar: FC<{}> = () => (
  <div style={{ backgroundColor: '#0086b4' }} className="rounded p-6">
    <span className="text-white uppercase font-semibold">queue</span>
  </div>
);

import React, { FC } from 'react';

import { FooterSidebar } from './FooterSidebar';
import { FooterContent } from './FooterContent';

export const Footer: FC<{}> = () => (
  <div className="flex">
    <div className="w-4/12">
      <FooterSidebar />
    </div>
    <div className="bg-white w-full">
      <FooterContent />
    </div>
  </div>
);

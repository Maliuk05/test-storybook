import React from 'react';

//types
import { ChatProps } from '../../types/common';

//custom component
import { HeaderInfo } from '../HeaderInfo';
import { MessagesList } from './MessagesList';
import { MessageControls } from './MessageControls';
import { IdCallUser } from '../IdCall/IdCallUser';

export const Chat = ({ data }: ChatProps) => {
  return (
    <div className="bg-gray-300 lg:bg-white py-4 px-4 lg:py-0 lg:px-0">
      <div className="block lg:hidden">
        <IdCallUser
          isArrowBack={true}
          svgColor1={'#434445'}
          svgColor2={'#434445'}
          svgColor3={'#434445'}
          colorText={'#434445'}
          backgroundColor={'#e2e8f0'}
          name={'John Doe'}
        />
      </div>
      <HeaderInfo />
      <div className="mt-12 mx-8 mb-4">
        {data.map((msg, index) => (
          <MessagesList data={msg.data} key={index} day={msg.date} />
        ))}
      </div>
      <hr />
      <MessageControls />
    </div>
  );
};

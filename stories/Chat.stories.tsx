import React from 'react';

//custom component
import {Chat} from '../src/components/Chat'

//types
import {ChatData} from "../src/types/common";

export default {
    title: 'Chat',
    component: Chat,
};

const messages: Array<ChatData> = [
    {
        date: 'Tuesday dec 17th 2020',
        data: [
            {
                username: 'Mrs.King',
                message:
                    'lorem dsfsdfds sdfsdfds sdfsdfsdfs dsfdsfdsf sfdsdfsdfds vsdfsdf sdfsdfsdf sdfsdfdsf dsfsdfsdf sdfsdf',
                time: '21:22',
                me: false,
            },
            {
                message: 'lorem dsfsdfds sdfsdfds',
                time: '21:22',
                me: true,
            },
        ],
    },
    {
        date: 'Tuesday dec 19th 2020',
        data: [
            {
                username: 'Mrs.King',
                message:
                    'lorem dsfsdfds sdfsdfds sdfsdfsdfs dsfdsfdsf sfdsdfsdfds vsdfsdf sdfsdfsdf',
                time: '19:22',
                me: false,
            },
            {
                message: 'lorem dsfsdfds',
                time: '20:15',
                me: true,
            },
        ],
    },
];

export const Default = () => {
    return (
        <div className='p-8 h-screen bg-white lg:bg-gray-300'>
            <div className='w-6/12 m-auto'>
                <Chat data={messages}/>
            </div>
        </div>
    );
};
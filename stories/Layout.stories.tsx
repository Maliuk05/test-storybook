import React from "react";

import {Layout} from "../src/components/Layout";

export default {
    title: 'Layout',
    component: Layout,
};

export const AllLayout = () => {
    return (
        <div>
            <Layout/>
        </div>
    )
};
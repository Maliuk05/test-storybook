# [1.0.0-beta.64](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.63...v1.0.0-beta.64) (2020-07-24)


### Features

* add Scores tab for [LS-10060] SEV View - Cricket ([#98](https://github.com/minelytix/livescore-react-components/issues/98)) ([0ae5936](https://github.com/minelytix/livescore-react-components/commit/0ae5936575d88ab4acc32a7abffabfef129e3586))

# [1.0.0-beta.63](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.62...v1.0.0-beta.63) (2020-07-24)


### Features

* [LS-10482] cricket match details info tab ([#94](https://github.com/minelytix/livescore-react-components/issues/94)) ([2de8e8e](https://github.com/minelytix/livescore-react-components/commit/2de8e8e09ba7504cf835a66abddb192862c32494))

# [1.0.0-beta.62](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.61...v1.0.0-beta.62) (2020-07-23)


### Bug Fixes

* improved types on match details ([#96](https://github.com/minelytix/livescore-react-components/issues/96)) ([5d976a9](https://github.com/minelytix/livescore-react-components/commit/5d976a9ab105177bbfbba48c8d51d151dfa57c8c))

# [1.0.0-beta.61](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.60...v1.0.0-beta.61) (2020-07-23)


### Features

* **ls-10568:** soccer widget ([#89](https://github.com/minelytix/livescore-react-components/issues/89)) ([5257faf](https://github.com/minelytix/livescore-react-components/commit/5257faf2d98c06f5623fe90c1e3527833e998910))

# [1.0.0-beta.60](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.59...v1.0.0-beta.60) (2020-07-22)


### Bug Fixes

* match table functionality to live site ([#93](https://github.com/minelytix/livescore-react-components/issues/93)) ([76b8d0d](https://github.com/minelytix/livescore-react-components/commit/76b8d0d3e35fcebf92bb9f6a7641cd616f6ac202))

# [1.0.0-beta.59](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.58...v1.0.0-beta.59) (2020-07-22)


### Features

* update components for [LS-10476] Create page in general and tabs component ([#91](https://github.com/minelytix/livescore-react-components/issues/91)) ([3b1e01e](https://github.com/minelytix/livescore-react-components/commit/3b1e01eedf38d7d70cc9c0fc40bee1db536056ba))

# [1.0.0-beta.58](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.57...v1.0.0-beta.58) (2020-07-22)


### Bug Fixes

* edited match detail story dimensions ([#92](https://github.com/minelytix/livescore-react-components/issues/92)) ([67a385e](https://github.com/minelytix/livescore-react-components/commit/67a385e60d7db61188785a8536716d3e5760ae38))

# [1.0.0-beta.57](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.56...v1.0.0-beta.57) (2020-07-22)


### Features

* adapted tables for multi tabs (division, conference, league) ([#90](https://github.com/minelytix/livescore-react-components/issues/90)) ([31fd47a](https://github.com/minelytix/livescore-react-components/commit/31fd47abaae581751bbd2ae3b44d56491f9d3599))

# [1.0.0-beta.56](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.55...v1.0.0-beta.56) (2020-07-21)


### Features

* **ls-10568:** move & refactor match details, remove tw, adjust code ([#84](https://github.com/minelytix/livescore-react-components/issues/84)) ([f2a78e5](https://github.com/minelytix/livescore-react-components/commit/f2a78e5655bb48533a0089c5adabaa100445e97b))

# [1.0.0-beta.55](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.54...v1.0.0-beta.55) (2020-07-20)


### Features

* [LS-10060] add favourite button ([#87](https://github.com/minelytix/livescore-react-components/issues/87)) ([f96bb25](https://github.com/minelytix/livescore-react-components/commit/f96bb25dca0e5138f6719e4d6612ef592a1926fc))

# [1.0.0-beta.54](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.53...v1.0.0-beta.54) (2020-07-20)


### Bug Fixes

* added event type of euro match rows ([#85](https://github.com/minelytix/livescore-react-components/issues/85)) ([6dc3a48](https://github.com/minelytix/livescore-react-components/commit/6dc3a4848e6ea0c00b1ea881e1291194d88584ee))

# [1.0.0-beta.53](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.52...v1.0.0-beta.53) (2020-07-17)


### Bug Fixes

* poc [LS-10538] widget creation ([#83](https://github.com/minelytix/livescore-react-components/issues/83)) ([82fb563](https://github.com/minelytix/livescore-react-components/commit/82fb5635beac33df128db73234aeed5ef0fda117))

# [1.0.0-beta.52](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.51...v1.0.0-beta.52) (2020-07-16)


### Bug Fixes

* Remove wrong emotion packages ([add1186](https://github.com/minelytix/livescore-react-components/commit/add118687f3590fb69597c3c2eb967347a0bb7ae))

# [1.0.0-beta.51](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.50...v1.0.0-beta.51) (2020-07-16)


### Bug Fixes

* Add mobile breakpoint laluga form ([167b6a3](https://github.com/minelytix/livescore-react-components/commit/167b6a39d3f246d0d0f6c771117ed1e0545d5a4d))

# [1.0.0-beta.50](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.49...v1.0.0-beta.50) (2020-07-16)


### Features

* [LS-10538] POC of Match Details Widget Creation ([#77](https://github.com/minelytix/livescore-react-components/issues/77)) ([b709b1c](https://github.com/minelytix/livescore-react-components/commit/b709b1c1b2e3433499874400774758a7b7138aca))

# [1.0.0-beta.49](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.48...v1.0.0-beta.49) (2020-07-16)


### Bug Fixes

* **ls-10530:** fix match calendar dates by tz ([#79](https://github.com/minelytix/livescore-react-components/issues/79)) ([a1ba183](https://github.com/minelytix/livescore-react-components/commit/a1ba183176b72a31474800fd80c9663a2f6ac8ef))

# [1.0.0-beta.48](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.47...v1.0.0-beta.48) (2020-07-15)


### Bug Fixes

* footer lint warnings and styles ([#78](https://github.com/minelytix/livescore-react-components/issues/78)) ([441b6f5](https://github.com/minelytix/livescore-react-components/commit/441b6f5ded343f80be8d5f54ce8eb686964f76fd))

# [1.0.0-beta.47](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.46...v1.0.0-beta.47) (2020-07-15)


### Bug Fixes

* remove type defs from components ([#75](https://github.com/minelytix/livescore-react-components/issues/75)) ([0d46218](https://github.com/minelytix/livescore-react-components/commit/0d462186a484db1332bd15481e0c1512bccd7623))

# [1.0.0-beta.46](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.45...v1.0.0-beta.46) (2020-07-14)


### Bug Fixes

* ui fixes for cricket MatchRow component ([#76](https://github.com/minelytix/livescore-react-components/issues/76)) ([8651fdc](https://github.com/minelytix/livescore-react-components/commit/8651fdc19223b5fea274ad1e0560cbcf6bdd9697))

# [1.0.0-beta.45](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.44...v1.0.0-beta.45) (2020-07-14)


### Features

* [LS-5755] Update header and footer ([#60](https://github.com/minelytix/livescore-react-components/issues/60)) ([b91050f](https://github.com/minelytix/livescore-react-components/commit/b91050f73310e4efc6eba59391621164848530b2))

# [1.0.0-beta.44](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.43...v1.0.0-beta.44) (2020-07-14)


### Bug Fixes

* **ls-10283:** fix active calendar ([#74](https://github.com/minelytix/livescore-react-components/issues/74)) ([814aaff](https://github.com/minelytix/livescore-react-components/commit/814aaff1924d59fd7458cf4adcb85bebd48a21a4))

# [1.0.0-beta.43](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.42...v1.0.0-beta.43) (2020-07-14)


### Features

* Added history back button [LS-5767] ([#70](https://github.com/minelytix/livescore-react-components/issues/70)) ([79068b7](https://github.com/minelytix/livescore-react-components/commit/79068b7f50f67c9c58e94e4da54a4f3dc02c5c37))

# [1.0.0-beta.42](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.41...v1.0.0-beta.42) (2020-07-13)


### Features

* **ls-10510:** add unit tests for DatePicker, Dates Utils, Select, TimeZoneSelect ([#72](https://github.com/minelytix/livescore-react-components/issues/72)) ([e832cf7](https://github.com/minelytix/livescore-react-components/commit/e832cf7f7efe39b707ca4756be3b5941cef373f0))

# [1.0.0-beta.41](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.40...v1.0.0-beta.41) (2020-07-10)


### Bug Fixes

* [LS-5740] Round Picker: added useEffect for handle state ([#71](https://github.com/minelytix/livescore-react-components/issues/71)) ([7a75f9b](https://github.com/minelytix/livescore-react-components/commit/7a75f9b53643f7db626e1158a55799725737b878))

# [1.0.0-beta.40](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.39...v1.0.0-beta.40) (2020-07-09)


### Bug Fixes

* LS-10023-update-news Update news list ([#69](https://github.com/minelytix/livescore-react-components/issues/69)) ([1beb198](https://github.com/minelytix/livescore-react-components/commit/1beb198e803399b5b3217c48bd5d758a7ae07579))

# [1.0.0-beta.39](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.38...v1.0.0-beta.39) (2020-07-09)


### Bug Fixes

* **ls-10344:** fix route change match calendar ([#66](https://github.com/minelytix/livescore-react-components/issues/66)) ([92e9c51](https://github.com/minelytix/livescore-react-components/commit/92e9c51f43de41ca3889fd8776f80b54a550e1d4))

# [1.0.0-beta.38](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.37...v1.0.0-beta.38) (2020-07-08)


### Features

* add loggin to debug component ([#67](https://github.com/minelytix/livescore-react-components/issues/67)) ([6ba9dbf](https://github.com/minelytix/livescore-react-components/commit/6ba9dbf5b46b492bd575dcc3838ef099517f2eef))

# [1.0.0-beta.37](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.36...v1.0.0-beta.37) (2020-07-07)


### Bug Fixes

* add euro StageFixturesList component ([#64](https://github.com/minelytix/livescore-react-components/issues/64)) ([78353b7](https://github.com/minelytix/livescore-react-components/commit/78353b71afcbea320ba07f6da788f07c2113d709))

# [1.0.0-beta.36](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.35...v1.0.0-beta.36) (2020-07-07)


### Features

* add euro StageFixturesList component ([#63](https://github.com/minelytix/livescore-react-components/issues/63)) ([96fd93f](https://github.com/minelytix/livescore-react-components/commit/96fd93f747cd3d1b9ef92445f785652a01225c7a))

# [1.0.0-beta.35](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.34...v1.0.0-beta.35) (2020-07-06)


### Features

* add euro components ([#62](https://github.com/minelytix/livescore-react-components/issues/62)) ([1ad249f](https://github.com/minelytix/livescore-react-components/commit/1ad249f7c5a8afce1bf0ac7ffd6e61545fc26fb3))

# [1.0.0-beta.34](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.33...v1.0.0-beta.34) (2020-07-03)


### Features

* **ls-10344:** add locale support to match calendar ([#59](https://github.com/minelytix/livescore-react-components/issues/59)) ([885bd67](https://github.com/minelytix/livescore-react-components/commit/885bd675f9199f34339c5417de0c5a832c5c7b13))

# [1.0.0-beta.33](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.32...v1.0.0-beta.33) (2020-07-02)


### Features

* **ls-10344:** add custom locale support and week start day to DateP… ([#55](https://github.com/minelytix/livescore-react-components/issues/55)) ([04f54d1](https://github.com/minelytix/livescore-react-components/commit/04f54d1ce71f95718f71f5f7710298caed9b25d7))

# [1.0.0-beta.32](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.31...v1.0.0-beta.32) (2020-07-02)


### Features

* added ability to pass custom tab handler to league table ([#58](https://github.com/minelytix/livescore-react-components/issues/58)) ([37dd98f](https://github.com/minelytix/livescore-react-components/commit/37dd98f3083ae3219354e9f32b516979ec02805d))

# [1.0.0-beta.31](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.30...v1.0.0-beta.31) (2020-07-01)


### Bug Fixes

* bug with active icon states [match calendar] ([#54](https://github.com/minelytix/livescore-react-components/issues/54)) ([0da5bc5](https://github.com/minelytix/livescore-react-components/commit/0da5bc57e034629c7a5334583461fab80087643e))

# [1.0.0-beta.30](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.29...v1.0.0-beta.30) (2020-07-01)


### Bug Fixes

* Change the year format ([3ed33ea](https://github.com/minelytix/livescore-react-components/commit/3ed33eae520eb0673c7a924ef4fce0ec12b063ad))

# [1.0.0-beta.29](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.28...v1.0.0-beta.29) (2020-06-30)


### Bug Fixes

* fix [LS-10110] MiniLeague table League link ([#53](https://github.com/minelytix/livescore-react-components/issues/53)) ([69f5458](https://github.com/minelytix/livescore-react-components/commit/69f5458c7523a71d01aed4cf32c3191893d55664))

# [1.0.0-beta.28](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.27...v1.0.0-beta.28) (2020-06-30)


### Features

* add [LS-5740] Round Picker ([#41](https://github.com/minelytix/livescore-react-components/issues/41)) ([f09b669](https://github.com/minelytix/livescore-react-components/commit/f09b669929084626da7161922993ee84a7efc386))

# [1.0.0-beta.27](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.26...v1.0.0-beta.27) (2020-06-30)


### Bug Fixes

* change Mini League table league link hover behavior ([#52](https://github.com/minelytix/livescore-react-components/issues/52)) ([c198649](https://github.com/minelytix/livescore-react-components/commit/c1986492a243490c0e76e7017259e3830919a7eb))

# [1.0.0-beta.26](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.25...v1.0.0-beta.26) (2020-06-30)


### Features

* added date label for mobile calendar ([#51](https://github.com/minelytix/livescore-react-components/issues/51)) ([ce2965c](https://github.com/minelytix/livescore-react-components/commit/ce2965cbc446a505190d8428b68e7485a80696a2))

# [1.0.0-beta.25](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.24...v1.0.0-beta.25) (2020-06-30)


### Bug Fixes

* add Mini League fixes ([#50](https://github.com/minelytix/livescore-react-components/issues/50)) ([c7684e8](https://github.com/minelytix/livescore-react-components/commit/c7684e8eac3df8ebc56cf3f3506d07664f40e383))

# [1.0.0-beta.24](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.23...v1.0.0-beta.24) (2020-06-30)


### Bug Fixes

* check for window before getting pathname ([#49](https://github.com/minelytix/livescore-react-components/issues/49)) ([05632b0](https://github.com/minelytix/livescore-react-components/commit/05632b0cb9176327d871d82d4dc492d79a089f09))

# [1.0.0-beta.23](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.22...v1.0.0-beta.23) (2020-06-30)


### Features

* added responsive date picker for mobile ([#48](https://github.com/minelytix/livescore-react-components/issues/48)) ([c392d84](https://github.com/minelytix/livescore-react-components/commit/c392d849a7351da21c301e77da8e541d8c820ab7))

# [1.0.0-beta.22](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.21...v1.0.0-beta.22) (2020-06-30)


### Bug Fixes

* fix path ([5e752c8](https://github.com/minelytix/livescore-react-components/commit/5e752c8ba9177b76a05cc911216455a886f075dd))


### Features

* update footer links ([8976f8d](https://github.com/minelytix/livescore-react-components/commit/8976f8d4d6555eca73400e656b3133e6f92d21e3))

# [1.0.0-beta.21](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.20...v1.0.0-beta.21) (2020-06-29)


### Features

* [LS-5756] Add TV Icon ([2be29a3](https://github.com/minelytix/livescore-react-components/commit/2be29a34dee3ce4fd3c8d1cc876e2a76a30db249))

# [1.0.0-beta.20](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.19...v1.0.0-beta.20) (2020-06-29)


### Bug Fixes

* Add country list ([532bef8](https://github.com/minelytix/livescore-react-components/commit/532bef8dabee05fca2107422b46f37f2bb44f70f))

# [1.0.0-beta.19](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.18...v1.0.0-beta.19) (2020-06-29)


### Bug Fixes

* **ls-10125:** fix calendar active bug ([d3e3ea9](https://github.com/minelytix/livescore-react-components/commit/d3e3ea94f14ccc738c4b75b3f909025ef2da7dba))
* **ls-10242:** styles fix ([73fa9d9](https://github.com/minelytix/livescore-react-components/commit/73fa9d96a5a38c82d20f3c88d799d3bf0b36184a))
* Change dateOfBirth format ([7ab094d](https://github.com/minelytix/livescore-react-components/commit/7ab094d7b91c7c443a9633071200a32f5d8b5660))
* Make laliga fields required ([db844cb](https://github.com/minelytix/livescore-react-components/commit/db844cbcd10fa5bbbe7b7d3d142ae380e2966442))
* Update README ([93ced4e](https://github.com/minelytix/livescore-react-components/commit/93ced4e6b140cf9364a978244af327ca49f596e8))

# [1.0.0-beta.18](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.17...v1.0.0-beta.18) (2020-06-26)


### Features

* add multicolors to minileague table ([#43](https://github.com/minelytix/livescore-react-components/issues/43)) ([0da2a86](https://github.com/minelytix/livescore-react-components/commit/0da2a86d075db46793e24d420b1763958f321585))

# [1.0.0-beta.17](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.16...v1.0.0-beta.17) (2020-06-26)


### Features

* add multicolor for league tables ([#42](https://github.com/minelytix/livescore-react-components/issues/42)) ([01fb226](https://github.com/minelytix/livescore-react-components/commit/01fb226082448a1c7b59a5242913d36450b18af9))

# [1.0.0-beta.16](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.15...v1.0.0-beta.16) (2020-06-25)


### Bug Fixes

* move i18n package to peer & refactor laliga ([1a6a9c5](https://github.com/minelytix/livescore-react-components/commit/1a6a9c5e1f36dcc889e2fbfad4bb3f6aaaabd5b4))

# [1.0.0-beta.15](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.14...v1.0.0-beta.15) (2020-06-25)


### Bug Fixes

* Fix LaLiga form layout ([a416d69](https://github.com/minelytix/livescore-react-components/commit/a416d699aa157dfdb1e94730f97d4822d79c5e66))

# [1.0.0-beta.14](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.13...v1.0.0-beta.14) (2020-06-25)


### Bug Fixes

* Ls 5796 match calendar ([#39](https://github.com/minelytix/livescore-react-components/issues/39)) ([7efa3f3](https://github.com/minelytix/livescore-react-components/commit/7efa3f38d9182490964c0f5fe9f5a9240574de8e))

# [1.0.0-beta.13](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.12...v1.0.0-beta.13) (2020-06-25)


### Bug Fixes

* add globals to tsdx config ([f54ad56](https://github.com/minelytix/livescore-react-components/commit/f54ad569a9b24b22614d265c3705b0ec11964465))

# [1.0.0-beta.12](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.11...v1.0.0-beta.12) (2020-06-25)


### Features

* Ls 5796 match calendar ([#27](https://github.com/minelytix/livescore-react-components/issues/27)) ([6267aaf](https://github.com/minelytix/livescore-react-components/commit/6267aafde422b516e5b08eaf9e960a61cb264526))

# [1.0.0-beta.11](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.10...v1.0.0-beta.11) (2020-06-24)


### Bug Fixes

* remove next link ([640059f](https://github.com/minelytix/livescore-react-components/commit/640059f957d422f90b3dc69bd50bbee014b1a01d))

# [1.0.0-beta.10](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.9...v1.0.0-beta.10) (2020-06-24)


### Bug Fixes

* align with i18n package ([#37](https://github.com/minelytix/livescore-react-components/issues/37)) ([d9bcb19](https://github.com/minelytix/livescore-react-components/commit/d9bcb1981fca7801b76fe499face3afd124fea85))

# [1.0.0-beta.9](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.8...v1.0.0-beta.9) (2020-06-24)


### Bug Fixes

* Fix umd file name ([0a26b66](https://github.com/minelytix/livescore-react-components/commit/0a26b66b3f4bc3416047d227c46a78c40266af21))

# [1.0.0-beta.8](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.7...v1.0.0-beta.8) (2020-06-24)


### Bug Fixes

* Remove bad dependencies ([ab7f18b](https://github.com/minelytix/livescore-react-components/commit/ab7f18bfefef89b7c5610bc388845f4dd52bbeaa))

# [1.0.0-beta.7](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.6...v1.0.0-beta.7) (2020-06-24)


### Bug Fixes

* update i18n to fallback missing translations ([9b1f225](https://github.com/minelytix/livescore-react-components/commit/9b1f225d632faea32ca3f3c391d3690fdaa9f8fb))

# [1.0.0-beta.6](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.5...v1.0.0-beta.6) (2020-06-23)


### Bug Fixes

* change Converting Snake Case to Camel Case ([#35](https://github.com/minelytix/livescore-react-components/issues/35)) ([2215883](https://github.com/minelytix/livescore-react-components/commit/22158838bddf0d6214d0af26babfc36750340c2f))


### Features

* Add LaLiga competition form ([#31](https://github.com/minelytix/livescore-react-components/issues/31)) ([7a750a0](https://github.com/minelytix/livescore-react-components/commit/7a750a05ccc87b448557a9fd9cdb9c8e818fd9f1))
* Feature/ls 5796 date picker ([#33](https://github.com/minelytix/livescore-react-components/issues/33)) ([402f408](https://github.com/minelytix/livescore-react-components/commit/402f408fc13c9d237205d7467c4d413e156fbe9f))

# [1.0.0-beta.5](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.4...v1.0.0-beta.5) (2020-06-19)


### Bug Fixes

* [LS-5761] Implement news social icons ([743b183](https://github.com/minelytix/livescore-react-components/commit/743b18325f09f65c99cac14e14f2afe30a6ec246))
* translate footer bottom links ([618411b](https://github.com/minelytix/livescore-react-components/commit/618411bf93ae6fb67f121d9108d5705a585d40ac))
* update i18n to fallback missing translations ([e79a52b](https://github.com/minelytix/livescore-react-components/commit/e79a52b9cab2c2874c34023a61ef1c265ba51474))

# [1.0.0-beta.4](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.3...v1.0.0-beta.4) (2020-06-18)


### Features

* **LS-5746:** Add League table component ([#25](https://github.com/minelytix/livescore-react-components/issues/25)) ([0da1979](https://github.com/minelytix/livescore-react-components/commit/0da197998d4887f3b238c113cf615396146e232b))

# [1.0.0-beta.3](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.2...v1.0.0-beta.3) (2020-06-18)


### Features

* [LS-5761] - Implement news social icons ([#28](https://github.com/minelytix/livescore-react-components/issues/28)) ([98c473f](https://github.com/minelytix/livescore-react-components/commit/98c473f268d848b5c489941cdc14f39d1a60bd71))
* add Footer component ([426bcc4](https://github.com/minelytix/livescore-react-components/commit/426bcc40d446e37e623734766b9ba9054c32c653))
* Add Notification component ([b4745e2](https://github.com/minelytix/livescore-react-components/commit/b4745e26f2bd007e9943db5e106381d8e8f02cc4))

# [1.0.0-beta.2](https://github.com/minelytix/livescore-react-components/compare/v1.0.0-beta.1...v1.0.0-beta.2) (2020-06-15)


### Features

* Add i18n from the npm package ([47c7212](https://github.com/minelytix/livescore-react-components/commit/47c72123ef5167e4034f79986f44b9ad36f79339))

# 1.0.0-beta.1 (2020-06-12)


### Bug Fixes

* **ci:** remove umd to decrease building time ([4b7024f](https://github.com/minelytix/livescore-react-components/commit/4b7024f10fc9cc03a0504bdf2ccc7160f3b4a1c0))


### Features

* initial commit # => v0.1.0 on [@latest](https://github.com/latest) ([8424090](https://github.com/minelytix/livescore-react-components/commit/84240909aeedc605cbbf5acd0840b87bf5edc64e))
